package sudoku;

public class Solver implements SudokuSolver{
	public int[][] grid;
	
	public Solver() {
		grid = new int[9][9];
	}
	
	public static void main(String[] args) {
		Solver s = new Solver();
		
		int[][] m = {
				  { 9, 0, 6, 0, 7, 0, 4, 0, 3 },
				  { 0, 0, 0, 4, 0, 0, 2, 0, 0 },
				  { 0, 7, 0, 0, 2, 3, 0, 1, 0 },
				  { 5, 0, 0, 0, 0, 0, 1, 0, 0 },
				  { 0, 4, 0, 2, 0, 8, 0, 6, 0 },
				  { 6, 0, 3, 0, 0, 0, 0, 0, 5 },
				  { 0, 3, 0, 7, 0, 0, 0, 5, 0 },
				  { 0, 0, 7, 0, 0, 5, 0, 0, 0 },
				  { 4, 0, 5, 0, 1, 0, 7, 0, 8 } 
		};
		s.setMatrix(m);
		
		s.solve();
		
		for(int[] i : m) {
			for(int j : i) {
				System.out.println(j);
			}
		}
	}
	
	@Override
	public boolean solve() {
		return solve(0, 0);
	}
	
	private Boolean solve(int r, int c) {
		if (r==9) {
			return true;
		} else {
			if (grid[r][c] == 0) {
				for (int i = 1; i < 10; i++) {
					grid[r][c] = i;
					if (isValid() && solve(nextRow(r, c), nextCol(r, c))) {
							return true;
						
					} else {
						grid[r][c] = 0;
					}
				}
			} else {
				if (isValid() && solve(nextRow(r, c), nextCol(r, c))) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}
	
	private int nextRow(int row, int col) {
		if(col == 8) {
			return row+1;
		}else {
			return row;
		}
	}
	
	private int nextCol(int row, int col) {
		if(col == 8) {
			return 0;
		}else {
			return col+1;
		}
	}

	@Override
	public void add(int row, int col, int digit) {
		if(row > 8 || row < 0 || col > 8 || col < 0 || digit > 9 || digit < 0) {
			throw new IllegalArgumentException();
		}else {
			grid[row][col] = digit;
		}
	}

	@Override
	public void remove(int row, int col) {
		if(row > 8 || row < 0 || col > 8 || col < 0) {
			throw new IllegalArgumentException();
		}else {
			grid[row][col] = 0;
			if(col == 0) {
				row --;
				col = 9;
			}else {
				col --;
			}
		}
	}

	@Override
	public int get(int row, int col) {
		if(row > 8 || row < 0 || col > 8 || col < 0) {
			throw new IllegalArgumentException();
		}else {
		return grid[row][col];
		}
	}

	@Override
	public void clear() {
		grid = null;
	}

	@Override
	public void setMatrix(int[][] m) {
		IllegalArgumentException exc = new IllegalArgumentException();
		if(m.length > 9 || m[0].length > 9) {
			throw exc;
		}for(int[] a : m){
			for(int i : a) {
				if(i > 9 || i < 0) {
					throw exc;
				}
			}
		}
		grid = m;
	}

	@Override
	public int[][] getMatrix() {
		return grid;
	}

	@Override
	public boolean isValid() {
		if(rowValidate() && colValidate() && boxValidate()) {
			return true;
		}return false;
	}
	
	private boolean rowValidate() {
		for(int row = 0; row < 9; row++) {
			int sum = 0;
			for(int col = 0; col < 9; col++) {
				sum += grid[row][col];
			}if(sum > 45) {
				return false;
			}
		}
		for(int row = 0; row < 9; row++) {
			for(int col = 0; col < 9; col++) {
				for(int c = col+1; c < 9; c++) {
					if(grid[row][c] == grid[row][col] && grid[row][c] != 0){
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private boolean colValidate() {
		for(int col = 0; col < 9; col++) {
			int sum = 0;
			for(int row = 0; row < 9; row++) {
				sum += grid[row][col];
			}if(sum > 45) {
				return false;
			}
		}
		
		for(int col = 0; col < 9; col++) {
			for(int row = 0; row < 9; row++) {
				for(int r = row+1; r < 9; r++) {
					if(grid[r][col] == grid[row][col] && grid[r][col] != 0){
						return false;
					}
				}
			}
		}return true;
	}
	
	private boolean boxValidate() {
		for(int r = 0; r < 9; r+=3) {
			for(int c = 0; c < 9; c+=3) {
				int sum = 0;
				//Hamnar i början på varje box här
				for(int row = r; row < r+3; row ++) {
					for(int col = c; col < c+3; col++) {
						sum += grid[row][col];
					}
				}if(sum > 45) {
					return false;
				}
			}
		}
		
		for(int r = 0; r < 9; r+=3) {
			for(int c = 0; c < 9; c+=3) {
				for(int row = r; row < r+3; row ++) {
					for(int col = c; col < c+3; col++) {
						for(int col2 = col+1; col2 < c+3; col2++) {
							if(grid[row][col2] == grid[row][col] && grid[row][col2] != 0) {
								return false;
							}
						}
					}
				}
			}
		}return true;
	}
}
