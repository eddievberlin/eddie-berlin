package sudoku;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SolverTest {
	private Solver s;

	@BeforeEach
	void setUp() throws Exception {
		this.s = new Solver();
	}

	@AfterEach
	void tearDown() throws Exception {
		s.clear();
	}
	
	@Test 
	void testAdd() {
		s.add(0, 0, 1);
		assertEquals(1, s.get(0, 0), "Add does not work porperly");
	}
	
	@Test 
	void testRemove() {
		s.add(0, 0, 1);
		s.remove(0, 0);
		assertEquals(0, s.get(0, 0), "The remove method does not work properly");
	}
	
	
	
	@Test
	void testSolveSudoku() {
		Solver s = new Solver();
		
		int[][] m = {
				  { 0, 0, 8, 0, 0, 9, 0, 6, 2 },
				  { 0, 0, 0, 0, 0, 0, 0, 0, 5 },
				  { 1, 0, 2, 5, 0, 0, 0, 0, 0 },
				  { 0, 0, 0, 2, 1, 0, 0, 9, 0 },
				  { 0, 5, 0, 0, 0, 0, 6, 0, 0 },
				  { 6, 0, 0, 0, 0, 0, 0, 2, 8 },
				  { 4, 1, 0, 6, 0, 8, 0, 0, 0 },
				  { 8, 6, 0, 0, 3, 0, 1, 0, 0 },
				  { 0, 0, 0, 0, 0, 0, 4, 0, 0 } 
		};
		s.setMatrix(m);
		
		assertTrue(s.solve());
	}
		
	
	@Test
	void testIllegalArgument() {
		assertThrows(IllegalArgumentException.class, () -> s.add(1, 1, 10), "Should not be able to type in 10");
		assertThrows(IllegalArgumentException.class, () -> s.add(1, 9, 2), "Col 9 Illegal");
		assertThrows(IllegalArgumentException.class, () -> s.add(10, 8, 2), "Row 10 Illegal");
	}
		
	@Test
	void testIsValid() {
		Solver s = new Solver();
		int[][] m = {
				  { 0, 0, 8, 0, 0, 9, 0, 6, 2 },
				  { 0, 0, 0, 0, 0, 0, 0, 0, 5 },
				  { 1, 0, 2, 5, 0, 0, 0, 0, 0 },
				  { 0, 0, 0, 2, 1, 0, 0, 9, 0 },
				  { 0, 5, 0, 0, 0, 0, 6, 0, 0 },
				  { 6, 0, 0, 0, 0, 0, 0, 2, 8 },
				  { 4, 1, 0, 6, 0, 8, 0, 0, 0 },
				  { 8, 6, 0, 0, 3, 0, 1, 0, 0 },
				  { 0, 0, 0, 0, 0, 0, 4, 0, 0 } 
		};
		
		int[][] n = new int[9][9];
		
		for(int[] i : n) {
			for(int j : i) {
				j = 9;
			}
		}
		s.setMatrix(m);
		
		assertTrue(s.isValid());
	}
	
	@Test
	void testGet() {
		s.add(0, 0, 1);
		assertEquals(1, s.get(0, 0), "The get method did not return the correct number");
	}
	
	@Test 
	void testgetMatrix() {
		int[][] matrix = new int[9][9];
		matrix[0][0] = 1;
		
		s.add(0, 0, 1);
		assertTrue(matrix[0][0] == (s.getMatrix()[0][0]), "The getMatrix method did not return the correct matrix");
	}
	@Test 
	void testsetMatrix() {
		int[][] matrix = new int[9][9];
		matrix[0][0] = 1;
		s.setMatrix(matrix);
		assertTrue(matrix.equals(s.getMatrix()), "The setMatrix method did not set the matrix correctly");
	}
}
