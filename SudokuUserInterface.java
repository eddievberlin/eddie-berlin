package sudoku;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class SudokuUserInterface {
	JTextField[][] boxes;
	SudokuSolver p;
	JButton b1;
	JButton b2;
	JButton b3;

	public SudokuUserInterface(SudokuSolver s) {
		SwingUtilities.invokeLater(() -> createWindow(s, "Sudoku", 300, 300));
	}

	void createWindow(SudokuSolver s, String title, int width, int height) {
		JFrame frame = new JFrame(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
		JPanel panel = new JPanel();
		b1 = new JButton("Lös");
		b2 = new JButton("Rensa");
		b3 = new JButton("Generera soduko");
		b1.addActionListener(w -> solve());
		b2.addActionListener(w -> clear());
		panel.add(b1);
		panel.add(b2);
		JPanel panel2 = new JPanel();
		GridLayout grid = new GridLayout(9, 9);
		panel2.setLayout(grid);
		p = new Solver();
		JPanel p1 = new JPanel();
		p1.setLayout(grid);
		JPanel p2 = new JPanel();
		p2.setLayout(grid);
		Font font = new Font("Font", Font.BOLD, 20);
		this.boxes = new JTextField[9][9];
		for (int r = 0; r < 9; r++) {
			for (int c = 0; c < 9; c++) {
				boxes[r][c] = new JTextField(1);
				boxes[r][c].setHorizontalAlignment(JTextField.CENTER);
				boxes[r][c].setFont(font);
				panel2.add(boxes[r][c]);
			}
		}
		for (int a = 0; a < 7; a = a + 6) {
			for (int d = 0; d < 3; d++) {
				for (int c = 0; c < 7; c = c + 6) {
					for (int b = 0; b < 3; b++) {
						boxes[a + d][c + b].setBackground(new Color(236, 152, 23));
					}
				}
			}
		}
		for (int d = 3; d < 6; d++) {
			for (int b = 3; b < 6; b++) {
				boxes[d][b].setBackground(new Color(236, 152, 23));
			}
		}

		frame.add(panel2, BorderLayout.CENTER);
		frame.add(panel, BorderLayout.SOUTH);
		frame.setBounds(50, 50, 480, 500);
	}

	public void solve() {
		try {
			p.setMatrix(getIntMatrix());
			if (p.solve()) {
				setMatrix(p.getMatrix());
			} else if (!p.solve()) {
				JOptionPane.showMessageDialog(null,
						"Detta sudoku går inte att lösa. Testa att byta en eller flera siffror");
			}
		} catch (Exception IllegalArgumentException) {
			JOptionPane.showMessageDialog(null, "Du får bara skriva in siffror mellan 1-9");
		}

	}

	public int get(int row, int col) {
		if (boxes[row][col].getText().equals("0")) {
			throw new IllegalArgumentException();
		}
		if (boxes[row][col].getText().equals("")) {
			return 0;
		} else {
			return Integer.valueOf(boxes[row][col].getText()).intValue();
		}
	}

	public void clear() {
		for (int r = 0; r < 9; r++) {
			for (int c = 0; c < 9; c++) {
				boxes[r][c].setText("");
			}
		}
	}

	public void setMatrix(int[][] m) {
		for (int r = 0; r < 9; r++) {
			for (int c = 0; c < 9; c++) {
				boxes[r][c].setText(Integer.valueOf(m[r][c]).toString());
			}
		}
	}

	public int[][] getIntMatrix() {
		int[][] intMatrix = new int[9][9];
		for (int r = 0; r < 9; r++) {
			for (int c = 0; c < 9; c++) {
				intMatrix[r][c] = get(r, c);
			}
		}
		return intMatrix;
	}
}
